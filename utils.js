$(document).ready(function() {

    utils = {};

    /**
     * Form Functions
     */
    utils.form = {
        /**
         * Show Form Message.
         * @param $prependOn
         * @param $class
         * @param $message
         * @param $interval
         */
        showMessage: function($prependOn, $class, $message, $interval) {
            var template = '<div class="alert $class" style="display: none;">$message</div>'.replace('$class', $class).replace('$message', $message);
            $prependOn.prepend(template);

            var message = $prependOn.find('.'+$class);
            message.fadeIn();
            setTimeout(function() {
                message.fadeOut();
            }, $interval ? $interval : 3000);
        },

        /**
         * Parse form fields with data-name attribute converting them to an object.
         * @param $wrapper
         *
         * @returns {{}}
         */
        parseByDataName: function($wrapper) {
            var obj = {};
            var last, key;

            $.each($wrapper.find('input[type=text], input[type=email], input[type=number], input[type=hidden], select, textarea, checkbox, radio'), function() {
                if($(this).attr('data-name')) {
                    last = obj;
                    key  = $(this).attr('data-name').split('.');

                    for(var i in key) {
                        if(!last[key[i]]) {
                            last[key[i]] = {};
                        }
                        if(i == key.length-1) {
                            last[key[i]] = $(this).val();
                        }

                        last = last[key[i]];
                    }
                }
            });

            return obj;
        }
    };

    /**
     * Datatable Functions
     */
    utils.datatable = {
        /**
         * Set Datatable Defaults
         */
        setDefaults: function() {
            $.extend(true, $.fn.dataTable.defaults, {
                "iDisplayLength": 10,
                "bAutoWidth": false,
                "bDestroy": true,
                "processing": true,
                "serverSide": true,
                "sDom": "<'row'<'span8'><'span8'f>r>t<'row'<'span8'l><'span8'p>><'span8'i>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page",
                    "sSearch": ""
                },
                "fnPreDrawCallback": function() {
                    $('.dataTables_processing').hide();
                    $('#loader').fadeIn();
                    $('#mask').fadeIn();
                },
                "fnDrawCallback": function() {
                    $('#loader').fadeOut();
                    $('#mask').fadeOut();
                }
            });
        },

        /**
         * Fix Search CSS of Data Tables
         */
        fixFilterDisplay: function() {
            $('.dataTables_filter input').addClass('form-control').attr('placeholder','Search...');
            $('.dataTables_length select').addClass('form-control');
        },

        /**
         * Generates the Column Structure for the Data Tables
         *
         * @param columns
         * @returns {Array}
         */
        generateColumns: function(columns) {
            finalColumn = [];

            for(var i in columns) {
                finalColumn.push({ title: columns[i] });
            }

            return finalColumn;
        }
    };
});